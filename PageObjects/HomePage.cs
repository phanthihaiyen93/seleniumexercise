﻿using OpenQA.Selenium;
using SeleniumExercise.Common;

namespace SeleniumExercise.PageObjects
{
    class HomePage: BasePage
    {
        private By loginButton = By.Id("login");
        private By usernameLabel = By.Id("userName-value");
        private By closeAdsArrowButton = By.Id("close-fixedban");
        private By searchTextBox = By.Id("searchBox");
        private By searchButton = By.Id("basic-addon2");

        public HomePage(IWebDriver driver) : base(driver) 
        { 
        }

        public LoginPage GoToLoginPage()
        {
            ClickToElement(loginButton);
            return new LoginPage(driver);
        }

        public string GetUsernameLabelValue()
        {
            WaitForElementVisible(usernameLabel);
            return GetElementText(usernameLabel);
        }

        public void CloseAdsPopup()
        {
            ClickToElement(closeAdsArrowButton);
        }

        public void SelectBook(By book)
        {
            WaitForElementVisible(book);
            ClickToElement(book);
        }

        public void Search(string searchContent)
        {
            WaitForElementVisible(searchTextBox);
            SendKeyToElement(searchTextBox, searchContent);
            ClickToElement(searchButton);
        }
    }
}
