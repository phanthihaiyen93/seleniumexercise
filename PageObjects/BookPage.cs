﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExercise.Common;
using System;

namespace SeleniumExercise.PageObjects
{
    class BookPage : BasePage
    {
        private By addToCollectionButton = By.XPath("//button[contains(text(),'Add To Your Collection')]");
        private By closeAdsArrowButton = By.Id("close-fixedban");

        public BookPage(IWebDriver driver) : base(driver)
        {
        }

        public void AddBookToCollection()
        {
            CloseAdsPopup();

            WaitForElementExists(addToCollectionButton);
            IWebElement element = FindElement(addToCollectionButton);

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            ClickToElement(addToCollectionButton);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());

            IAlert simpleAlert = driver.SwitchTo().Alert();
            simpleAlert.Accept();
        }

        public void CloseAdsPopup()
        {
            WaitForElementVisible(closeAdsArrowButton);
            ClickToElement(closeAdsArrowButton);
        }
    }
}
