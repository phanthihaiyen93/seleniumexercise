﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExercise.Common;
using SeleniumExercise.PageObjects;

namespace SeleniumExercise.TestCases
{
    class SearchBookWithMultipleResultTests: WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("chrome");
            driver.Navigate().GoToUrl(Common.Constant.APP_URL);
        }

        [Test]
        public void AddBookToCollection()
        {
            // Login
            homePage = new HomePage(driver);
            homePage.CloseAdsPopup();
            loginPage = homePage.GoToLoginPage();
            homePage = loginPage.LoginWithValidAccount(Constant.USERNAME2, Constant.PASSWORD2);

            // search 
            homePage.Search("design");

            // assert results
            homePage.WaitForElementVisible(By.XPath("//div[contains(@class,'rt-tbody')]"));
            By book1 = By.XPath("//a[contains(text(),'Learning JavaScript Design Patterns')]");
            By book2 = By.XPath("//a[contains(text(),'Designing Evolvable Web APIs with ASP.NET')]");

            var isExistBook1 = driver.FindElement(book1).Displayed;
            var isExistBook2 = driver.FindElement(book2).Displayed;

            Assert.AreEqual(true, isExistBook1 && isExistBook2);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
