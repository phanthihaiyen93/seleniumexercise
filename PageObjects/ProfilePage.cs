﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExercise.Common;
using System;
using System.Linq;

namespace SeleniumExercise.PageObjects
{
    class ProfilePage : BasePage
    {
        private By deleteButton = By.XPath("//span[contains(@id,'delete-record-undefined')]");
        private By table = By.XPath("//div[contains(@class,'rt-tbody')]");
        private By OKButtonOnModal = By.Id("closeSmallModal-ok");

        public ProfilePage(IWebDriver driver) : base(driver)
        {
        }

        public bool IsExistInBookCollection(By book)
        {
            WaitForElementVisible(table);
            var element = driver.FindElements(book);
            return element.Any() && element[0].Displayed;
        }

        public void DeleteBook()
        {
            ClickToElement(deleteButton);

            WaitForElementVisible(OKButtonOnModal);
            ClickToElement(OKButtonOnModal);

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent());
            IAlert simpleAlert = driver.SwitchTo().Alert();
            simpleAlert.Accept();
        }
    }
}
