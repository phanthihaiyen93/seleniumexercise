﻿using OpenQA.Selenium;
using SeleniumExercise.Common;

namespace SeleniumExercise.PageObjects
{
    class LoginPage : BasePage
    {
        private By userNameTextBox = By.Id("userName");
        private By passwordTextBox = By.Id("password");
        private By loginButton = By.Id("login");
        private By invalidLoginMessage = By.Id("name");

        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        public HomePage LoginWithValidAccount(string userName, string password)
        {
            SendKeyToElement(userNameTextBox, userName);
            SendKeyToElement(passwordTextBox, password);
            ClickToElement(loginButton);
            return new HomePage(driver);
        }

        public void LoginWithInvalidAccount(string userName, string password)
        {
            SendKeyToElement(userNameTextBox, userName);
            SendKeyToElement(passwordTextBox, password);
            ClickToElement(loginButton);
        }

        public string GetInvalidLoginMessage()
        {
            WaitForElementVisible(invalidLoginMessage);
            return GetElementText(invalidLoginMessage);
        }
    }
}
