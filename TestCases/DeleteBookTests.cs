﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExercise.Common;
using SeleniumExercise.PageObjects;

namespace SeleniumExercise.TestCases
{
    class DeleteBookTests : WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;
        BookPage bookPage;
        ProfilePage profilePage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("chrome");
            driver.Navigate().GoToUrl(Common.Constant.APP_URL);
        }

        [Test]
        public void DeleteBookInCollectionSuccessfully()
        {
            // Login
            homePage = new HomePage(driver);
            homePage.CloseAdsPopup();
            loginPage = homePage.GoToLoginPage();
            homePage = loginPage.LoginWithValidAccount(Constant.USERNAME2, Constant.PASSWORD2);

            // select and add a book to collection
            By book = By.XPath("//a[contains(text(),'Git Pocket Guide')]");
            homePage.SelectBook(book);
            bookPage = new BookPage(driver);
            bookPage.AddBookToCollection();

            // Check if the book exists on Profile page and delete it
            driver.Navigate().GoToUrl("https://demoqa.com/profile");
            profilePage = new ProfilePage(driver);

            if (profilePage.IsExistInBookCollection(book))
            {
                profilePage.DeleteBook();
                var result = profilePage.IsExistInBookCollection(book);
                Assert.AreEqual(false, result, "Delete not successfully!");
            }
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
