﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExercise.Common;
using SeleniumExercise.PageObjects;

namespace SeleniumExercise.TestCases
{
    class LoginTests: WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("chrome");
            driver.Navigate().GoToUrl(Common.Constant.APP_URL);
        }

        [Test]
        public void LoginWithValidUser()
        {
            string validUsername = Common.Constant.USERNAME2;
            string validUPassword = Common.Constant.PASSWORD2;

            homePage = new HomePage(driver);
            homePage.CloseAdsPopup();
            loginPage = homePage.GoToLoginPage();
            homePage = loginPage.LoginWithValidAccount(validUsername, validUPassword);

            string headerText = homePage.GetUsernameLabelValue();
            Assert.AreEqual(headerText, validUsername, "Username is not displayed as expexted.");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
