﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;

namespace SeleniumExercise.Common
{
    class BasePage
    {
        public IWebDriver driver;
        private IWebElement element;
        IList<IWebElement> elements;
        private WebDriverWait explicitWait;
        private readonly long timeOut = 30;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void OpenUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public IWebElement FindElement(By byLocator)
        {
            return driver.FindElement(byLocator);
        }

        public IList<IWebElement> FindElements(By byLocator)
        {
            return driver.FindElements(byLocator);
        }

        public void ClickToElement(By byLocator)
        {
            this.FindElement(byLocator).Click();
        }

        public void SendKeyToElement(By byLocator, string value)
        {
            element = this.FindElement(byLocator);
            element.Clear();
            element.SendKeys(value);
        }

        public void WaitForElementVisible(By byLocator)
        {
            explicitWait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeOut));
            explicitWait.Until(ExpectedConditions.ElementIsVisible(byLocator));
        }

        public void WaitForElementExists(By byLocator)
        {
            explicitWait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeOut));
            explicitWait.Until(ExpectedConditions.ElementExists(byLocator));
        }

        public string GetElementText(By byLocator)
        {
            return this.FindElement(byLocator).Text;
        }

        public string GetElementText(IWebElement element)
        {
            return element.Text;
        }
    }
}
