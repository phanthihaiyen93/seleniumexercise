﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExercise.Common;
using SeleniumExercise.PageObjects;

namespace SeleniumExercise.TestCases
{
    class AddBookToCollectionTests : WebDriverManagers
    {
        IWebDriver driver;
        HomePage homePage;
        LoginPage loginPage;
        ProfilePage profilePage;
        BookPage bookPage;

        [SetUp]
        public void Setup()
        {
            driver = CreateBrowserDriver("chrome");
            driver.Navigate().GoToUrl(Common.Constant.APP_URL);
        }

        [Test]
        public void AddBookToCollection()
        {
            // Login
            homePage = new HomePage(driver);
            homePage.CloseAdsPopup();
            loginPage = homePage.GoToLoginPage();
            homePage = loginPage.LoginWithValidAccount(Constant.USERNAME2, Constant.PASSWORD2);

            // select the book
            By book = By.XPath("//a[contains(text(),'Git Pocket Guide')]");
            homePage.SelectBook(book);

            // add the book to collection
            bookPage = new BookPage(driver);
            bookPage.AddBookToCollection();

            // Check on Profile page
            driver.Navigate().GoToUrl("https://demoqa.com/profile");
            profilePage = new ProfilePage(driver);
            var result = profilePage.IsExistInBookCollection(book);

            Assert.AreEqual(true, result, "Book does not exist in collection");
        }

        [TearDown]
        public void TearDown()
        {
            profilePage.DeleteBook();
            driver.Quit();
        }
    }
}
